# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

# load modules
define LOAD_MODULES
	module load assemblers/SOAPdenovo/r240-bin; \
	module load sw/assemblers/gam-ngs/default
endef


# could be all, sparse_pregraph, pregraph
PIPELINE := all
# define variable =yes if you want error correction.
# in this case remember to add .cor.fq.gz to sequence file name in the SOAP configuration file
EC := no

# Project Name
PRJ_NAME := rpv3
# kmer to start from in the Multi-Kmer approach
KMER := 63
# kmer to end from in the Multi-Kmer approach
MAXKMER := 96
# reference genome
REFERENCE := /mnt/vol1/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta.gz
# sequencing length for filtering contigs and scaffolds
FILTER_LENGTH := 1000


# NULL := $(call set,,Q1,)
# NULL := $(call set,,Q2,)
# LIBS += 
# q1=$(call get,Q1).cor.fq.gz
# q2=$(call get,,Q2).cor.fq.gz
# 
# NULL := $(call set,,Q1,)
# LIBS += 
# q=$(call get,,Q1).cor.fq.gz


# options of abyss that can be defined by the user
define SOAP_VARIABLE_PART
	-R \   * resolve repeats by reads *
	-m $(MAXKMER) \   * maxKmer (max 127): maximum kmer size used for multi-kmer. The incremental step is set to 1 *
	-E \   * merge clean bubble before iterate, works only if -M is set when using multi-kmer *
	-F \   * fill gaps in scaffolds *
	-V   * output information for Hawkeye to visualize the assembly *
endef



# options of KmerFreq_HA that can be defined by the user
define 	KMERFREQ_HA_VARIABLE_PART
	-k 24 \   * Set kmer size *
	-f 0 \   * Set whether use Bloom filter to reduce memory usage *
	-b 1 \   * Set the kmer-freq analysis divide into many steps *
	-i 50000000 \   * Set initial size of hash table *
	-L 500   * Set maximum read length *
endef

# options of Corrector_HA that can be defined by the user
define	CORRECTOR_HA_VARIABLE_PART
	-k 24 \   * Set kmer size *
	-l 2 \   * Kmers with frequency lower than(<=) this cutoff will be taken as suspicious Kmers *
	-e 1 \   * Set whether trim(1) suspicious region at the end of reads according to Q_value<=2 or not(0) *
	-w 1 \   * Set whether trim(1) error bases with Q_value<=2 instead of correct(0) it *
	-q 30 \   * Set Quality threshold of error bases *
	-r 50 \   * Set the minimum length of trimmed read *
	-Q 33 \   * Set the ASCII shift of the quality value(Quality_shift) *
	-o 1   * Set output file format *
endef

# initialize empty
LIBS :=

# PAIRED-ENDS
NULL := $(call set,rpv3_PE_exp188,Q1,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/paired_ends/trimmed/exp_188_homoz_rpv3_ATCACG_L006_1_80Mreads.fastq.gz)
NULL := $(call set,rpv3_PE_exp188,Q2,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/paired_ends/trimmed/exp_188_homoz_rpv3_ATCACG_L006_2_80Mreads.fastq.gz)
LIBS += rpv3_PE_exp188

NULL := $(call set,rpv3_paired_from_mates,Q1,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/mates_merged_sets_246-358/homoz-rpv3_GTCCGC_clean_pair_1.fastq.gz)
NULL := $(call set,rpv3_paired_from_mates,Q2,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/mates_merged_sets_246-358/homoz-rpv3_GTCCGC_clean_pair_2.fastq.gz)
LIBS += rpv3_paired_from_mates

# MISEQ
NULL := $(call set,rpv3_Miseq_500,Q1,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/miseq/trimmed/RPV3-500bp_S3_L001_1.fastq.gz)
NULL := $(call set,rpv3_Miseq_500,Q2,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/miseq/trimmed/RPV3-500bp_S3_L001_2.fastq.gz)
LIBS += rpv3_Miseq_500

NULL := $(call set,rpv3_Miseq_600,Q1,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/miseq/trimmed/RPV3-600bp_S4_L001_1.fastq.gz)
NULL := $(call set,rpv3_Miseq_600,Q2,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/miseq/trimmed/RPV3-600bp_S4_L001_2.fastq.gz)
LIBS += rpv3_Miseq_600

# MATE-PAIRS
NULL := $(call set,rpv3_mates,Q1,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/mates_merged_sets_246-358/mates3K_homoz-rpv3_GTCCGC_merge_clean_BOTH-MP_1.fastq.gz)
NULL := $(call set,rpv3_mates,Q2,/mnt/vol2/projects/grape_resistance/rpv3/lanes/sequences/dna/Sample_homoz_rpv3/mates_merged_sets_246-358/mates3K_homoz-rpv3_GTCCGC_merge_clean_BOTH-MP_2.fastq.gz)
LIBS += rpv3_mates


# 1. Use both paired and unpaired reads
# 2. Use paired-ends for both contig contruction and scaffolding due to lack of overlap
# 3. Use miseq only for contig costruction due to overlap
# 4. Use mate-pairs only for scaffolding

# trick to generate configuration file
# 1. prepare multi-line variable
############## CONFIG FILE ##############
define CONFIG_FILE
# Any read longer than max_rd_len will be cut to this length
max_rd_len=500

# PAIRED-ENDS
[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=300
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=0
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=3
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=1
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# paired reads in fastq format (also in gziped), read 1 file should always be followed by read 2 file
q1=$(call get,rpv3_PE_exp188,Q1)
q2=$(call get,rpv3_PE_exp188,Q2)


[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=300
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=0
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=3
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=1
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# paired reads in fastq format (also in gziped), read 1 file should always be followed by read 2 file
q1=$(call get,rpv3_paired_from_mates,Q1)
q2=$(call get,rpv3_paired_from_mates,Q2)

# MISEQ
[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=400
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=0
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=1
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# paired reads in fastq format (also in gziped), read 1 file should always be followed by read 2 file
q1=$(call get,rpv3_Miseq_500,Q1)
q2=$(call get,rpv3_Miseq_500,Q2)


[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=500
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=0
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=1
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# paired reads in fastq format (also in gziped), read 1 file should always be followed by read 2 file
q1=$(call get,rpv3_Miseq_600,Q1)
q2=$(call get,rpv3_Miseq_600,Q2)

# MATES
[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=2000
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=1
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=2
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=2
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# paired reads in fastq format (also in gziped), read 1 file should always be followed by read 2 file
q1=$(call get,rpv3_mates,Q1)
q2=$(call get,rpv3_mates,Q2)
endef
############## CONFIG FILE ##############

# 2. export the variable value as-is to the shell as an environment variable
export CONFIG_FILE
