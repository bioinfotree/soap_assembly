# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>

# P.S.: REMEMBER TO REVERSE SINGLE SEQUENCES PRODUCED BY MATES AFTER EC

# load modules
define LOAD_MODULES
	module load assemblers/SOAPdenovo/r240-bin; \
	module load sw/assemblers/gam-ngs/default
endef


# could be all, sparse_pregraph, pregraph
PIPELINE := all
# define variable =yes if you want error correction.
# in this case remember to add .cor.fq.gz to sequence file name in the SOAP configuration file
EC := yes

# attach extension
EXTENSION1 = 
EXTENSION2 =
EXTENSION_SINGLE =
EXTENSION_COR =
ifeq ($(EC),yes)
EXTENSION1 = .cor.pair_1.fq.gz
EXTENSION2 = .cor.pair_2.fq.gz
EXTENSION_SINGLE = .cor.single.fq.gz
EXTENSION_COR = .cor.fq.gz
endif


# Project Name
PRJ_NAME := cfranc
# kmer to start from in the Multi-Kmer approach
KMER := 63
# kmer to end from in the Multi-Kmer approach
MAXKMER := 96
# reference genome
REFERENCE := /mnt/vol1/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta.gz
# sequencing length for filtering contigs and scaffolds
FILTER_LENGTH := 1000



# options of abyss that can be defined by the user
define SOAP_VARIABLE_PART
	-R \   * resolve repeats by reads *
	-m $(MAXKMER) \   * maxKmer (max 127): maximum kmer size used for multi-kmer. The incremental step is set to 1 *
	-E \   * merge clean bubble before iterate, works only if -M is set when using multi-kmer *
	-F \   * fill gaps in scaffolds *
	-V   * output information for Hawkeye to visualize the assembly *
endef



# options of KmerFreq_HA that can be defined by the user
define KMERFREQ_HA_VARIABLE_PART
	-k 24 \   * Set kmer size *
	-f 0 \   * Set whether use Bloom filter to reduce memory usage *
	-b 1 \   * Set the kmer-freq analysis divide into many steps *
	-i 50000000 \   * Set initial size of hash table *
	-L 500   * Set maximum read length *
endef

# options of Corrector_HA that can be defined by the user
define CORRECTOR_HA_VARIABLE_PART
	-k 24 \   * Set kmer size *
	-l 3 \   * Kmers with frequency lower than(<=) this cutoff will be taken as suspicious Kmers *
	-e 1 \   * Set whether trim(1) suspicious region at the end of reads according to Q_value<=2 or not(0) *
	-w 1 \   * Set whether trim(1) error bases with Q_value<=2 instead of correct(0) it *
	-q 30 \   * Set Quality threshold of error bases *
	-r 50 \   * Set the minimum length of trimmed read *
	-Q 33 \   * Set the ASCII shift of the quality value(Quality_shift) *
	-o 1   * Set output file format *
endef

# initialize empty
LIB_PAIRED :=
LIB_UNPAIRED :=

# PAIRED-ENDS
NULL := $(call set,exp_193_cabernet_franc_Undetermined_L003,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/paired_end/trimmed/exp_193_cabernet_franc_Undetermined_L003_1.fastq.gz)
NULL := $(call set,exp_193_cabernet_franc_Undetermined_L003,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/paired_end/trimmed/exp_193_cabernet_franc_Undetermined_L003_2.fastq.gz)
LIB_PAIRED += exp_193_cabernet_franc_Undetermined_L003

NULL := $(call set,exp_193_cabernet_franc_Undetermined_L003_unpaired,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/paired_end/trimmed/exp_193_cabernet_franc_Undetermined_L003_unpaired.fastq.gz)
LIB_UNPAIRED += exp_193_cabernet_franc_Undetermined_L003_unpaired

NULL := $(call set,exp_228_cabernet-franc_GCCAAT_L1,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_pair_1.fastq.gz)
NULL := $(call set,exp_228_cabernet-franc_GCCAAT_L1,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_pair_2.fastq.gz)
LIB_PAIRED += exp_228_cabernet-franc_GCCAAT_L1

NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L1,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_pair_1.fastq.gz)
NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L1,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_pair_2.fastq.gz)
LIB_PAIRED += exp_397_cabernet-franc_GCCAAT_L1

NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L2,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_pair_1.fastq.gz)
NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L2,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_pair_2.fastq.gz)
LIB_PAIRED += exp_397_cabernet-franc_GCCAAT_L2

# MISEQ
NULL := $(call set,exp_40_cabernet-franc_TCGCCTTA_M01598,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_40_cabernet-franc_TCGCCTTA_M01598_filtered_1.fastq.gz)
NULL := $(call set,exp_40_cabernet-franc_TCGCCTTA_M01598,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_40_cabernet-franc_TCGCCTTA_M01598_filtered_2.fastq.gz)
LIB_PAIRED += exp_40_cabernet-franc_TCGCCTTA_M01598

NULL := $(call set,exp_40_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_40_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz)
LIB_UNPAIRED += exp_40_cabernet-franc_TCGCCTTA_M01598_merged_unpaired

NULL := $(call set,exp_43_cabernet-franc_TCGCCTTA_M01598,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_43_cabernet-franc_TCGCCTTA_M01598_filtered_1.fastq.gz)
NULL := $(call set,exp_43_cabernet-franc_TCGCCTTA_M01598,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_43_cabernet-franc_TCGCCTTA_M01598_filtered_2.fastq.gz)
LIB_PAIRED += exp_43_cabernet-franc_TCGCCTTA_M01598

NULL := $(call set,exp_43_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_43_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz)
LIB_UNPAIRED += exp_43_cabernet-franc_TCGCCTTA_M01598_merged_unpaired

NULL := $(call set,exp_51_cabernet-franc_TCGCCTTA_M01598,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_51_cabernet-franc_TCGCCTTA_M01598_filtered_1.fastq.gz)
NULL := $(call set,exp_51_cabernet-franc_TCGCCTTA_M01598,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_51_cabernet-franc_TCGCCTTA_M01598_filtered_2.fastq.gz)
LIB_PAIRED += exp_51_cabernet-franc_TCGCCTTA_M01598

NULL := $(call set,exp_51_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_51_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz)
LIB_UNPAIRED += exp_51_cabernet-franc_TCGCCTTA_M01598_merged_unpaired

NULL := $(call set,exp_54_cabernet-franc_TCGCCTTA_M01598,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_54_cabernet-franc_TCGCCTTA_M01598_filtered_1.fastq.gz)
NULL := $(call set,exp_54_cabernet-franc_TCGCCTTA_M01598,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_54_cabernet-franc_TCGCCTTA_M01598_filtered_2.fastq.gz)
LIB_PAIRED += exp_54_cabernet-franc_TCGCCTTA_M01598

NULL := $(call set,exp_54_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_54_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz)
LIB_UNPAIRED += exp_54_cabernet-franc_TCGCCTTA_M01598_merged_unpaired

NULL := $(call set,exp_66_cabernet-franc_TCGCCTTA_M01598,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_66_cabernet-franc_TCGCCTTA_M01598_filtered_1.fastq.gz)
NULL := $(call set,exp_66_cabernet-franc_TCGCCTTA_M01598,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_66_cabernet-franc_TCGCCTTA_M01598_filtered_2.fastq.gz)
LIB_PAIRED += exp_66_cabernet-franc_TCGCCTTA_M01598

NULL := $(call set,exp_66_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/miseq/trimmed/exp_66_cabernet-franc_TCGCCTTA_M01598_merged_unpaired.fastq.gz)
LIB_UNPAIRED += exp_66_cabernet-franc_TCGCCTTA_M01598_merged_unpaired

# MATE-PAIRS
NULL := $(call set,exp_228_cabernet-franc_GCCAAT_L1_clean_mate,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_mate_1.fastq.gz)
NULL := $(call set,exp_228_cabernet-franc_GCCAAT_L1_clean_mate,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_mate_2.fastq.gz)
LIB_PAIRED += exp_228_cabernet-franc_GCCAAT_L1_clean_mate

NULL := $(call set,exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified_1.fastq.gz)
NULL := $(call set,exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified_2.fastq.gz)
LIB_PAIRED += exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified

NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L1_clean_mate,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_mate_1.fastq.gz)
NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L1_clean_mate,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_mate_2.fastq.gz)
LIB_PAIRED += exp_397_cabernet-franc_GCCAAT_L1_clean_mate

NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified_1.fastq.gz)
NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified_2.fastq.gz)
LIB_PAIRED += exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified

NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L2_clean_mate,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_mate_1.fastq.gz)
NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L2_clean_mate,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_mate_2.fastq.gz)
LIB_PAIRED += exp_397_cabernet-franc_GCCAAT_L2_clean_mate

NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified,Q1,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified_1.fastq.gz)
NULL := $(call set,exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified,Q2,/mnt/vol1/projects/novabreed/lanes/sequences/dna/vitis_vinifera/cabernet_franc/mates_nextera/delinked/exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified_2.fastq.gz)
LIB_PAIRED += exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified


# 1. Use both paired and unpaired reads
# 2. Use paired-ends for both contig contruction and scaffolding due to lack of overlap
# 3. Use miseq only for contig costruction due to overlap
# 4. Use mate-pairs only for scaffolding

# trick to generate configuration file
# 1. prepare multi-line variable
############## CONFIG FILE ##############
define CONFIG_FILE
# Any read longer than max_rd_len will be cut to this length
max_rd_len=500

PAIRED-ENDS
[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=279
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=0
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=3
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=1
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# paired reads in fastq format (also in gziped), read 1 file should always be followed by read 2 file
q1=$(notdir $(call get,exp_193_cabernet_franc_Undetermined_L003,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_193_cabernet_franc_Undetermined_L003,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_193_cabernet_franc_Undetermined_L003,Q1))$(EXTENSION_SINGLE)


[LIB]
# the average insert size of this library or the peak value position in the insert size distribution 
avg_ins=
# reverse complement seq, should be set to forward-reverse=0, reverse-forward=1
reverse_seq=0
# use library: only for contig building=1, only for scaffolding=2, both=3
asm_flags=1
# the assembler will cut the reads from the current library to this length
rd_len_cutoff=
# in which order the reads are used while scaffolding. Libraries with the same "rank" are used at the same time during scaffold assembly
rank=
# the minimum number for paired-end reads and mate-pair reads needed in order to define a link.
# Default: paired-end=3 mate-pair=5
pair_num_cutoff=
# the minimun alignment length between a read and a contig required for a reliable read location.
# Default: paired-end=32, mate-pairs=35 
map_len=
# single end reads
q=$(notdir $(call get,exp_193_cabernet_franc_Undetermined_L003_unpaired,Q1))$(EXTENSION_COR)




[LIB]
avg_ins=400
reverse_seq=0
asm_flags=3
rd_len_cutoff=
rank=1
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1,Q1))$(EXTENSION_SINGLE)



[LIB]
avg_ins=400
reverse_seq=0
asm_flags=3
rd_len_cutoff=
rank=1
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1,Q1))$(EXTENSION_SINGLE)


[LIB]
avg_ins=400
reverse_seq=0
asm_flags=3
rd_len_cutoff=
rank=1
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2,Q1))$(EXTENSION_SINGLE)


# MISEQ

[LIB]
avg_ins=400
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_40_cabernet-franc_TCGCCTTA_M01598,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_40_cabernet-franc_TCGCCTTA_M01598,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_40_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1))$(EXTENSION_COR)


[LIB]
avg_ins=400
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_43_cabernet-franc_TCGCCTTA_M01598,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_43_cabernet-franc_TCGCCTTA_M01598,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_43_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1))$(EXTENSION_COR)


[LIB]
avg_ins=400
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_51_cabernet-franc_TCGCCTTA_M01598,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_51_cabernet-franc_TCGCCTTA_M01598,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_51_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1))$(EXTENSION_COR)



[LIB]
avg_ins=400
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_54_cabernet-franc_TCGCCTTA_M01598,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_54_cabernet-franc_TCGCCTTA_M01598,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_54_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1))$(EXTENSION_COR)



[LIB]
avg_ins=469
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_66_cabernet-franc_TCGCCTTA_M01598,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_66_cabernet-franc_TCGCCTTA_M01598,Q2))$(EXTENSION2)



[LIB]
avg_ins=
reverse_seq=0
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_66_cabernet-franc_TCGCCTTA_M01598_merged_unpaired,Q1))$(EXTENSION_COR)

# MATE-PAIRS

[LIB]
avg_ins=2562
reverse_seq=1
asm_flags=2
rd_len_cutoff=
rank=2
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1_clean_mate,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1_clean_mate,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=1
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1_clean_mate,Q1))$(EXTENSION_SINGLE)


[LIB]
avg_ins=2562
reverse_seq=1
asm_flags=2
rd_len_cutoff=
rank=2
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=1
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_228_cabernet-franc_GCCAAT_L1_clean_unclassified,Q1))$(EXTENSION_SINGLE)


[LIB]
avg_ins=2562
reverse_seq=1
asm_flags=2
rd_len_cutoff=
rank=2
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1_clean_mate,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1_clean_mate,Q2))$(EXTENSION2)

[LIB]
avg_ins=
reverse_seq=1
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1_clean_mate,Q1))$(EXTENSION_SINGLE)



[LIB]
avg_ins=2562
reverse_seq=1
asm_flags=2
rd_len_cutoff=
rank=2
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=1
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L1_clean_unclassified,Q1))$(EXTENSION_SINGLE)

[LIB]
 
avg_ins=2562
reverse_seq=1
asm_flags=2
rd_len_cutoff=
rank=2
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2_clean_mate,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2_clean_mate,Q2))$(EXTENSION2)


[LIB]
avg_ins=
reverse_seq=1
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2_clean_mate,Q1))$(EXTENSION_SINGLE)


[LIB]
avg_ins=2562
reverse_seq=1
asm_flags=2
rd_len_cutoff=
rank=2
pair_num_cutoff=
map_len=
q1=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified,Q1))$(EXTENSION1)
q2=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified,Q2))$(EXTENSION2)

[LIB]
avg_ins=
reverse_seq=1
asm_flags=1
rd_len_cutoff=
rank=
pair_num_cutoff=
map_len=
q=$(notdir $(call get,exp_397_cabernet-franc_GCCAAT_L2_clean_unclassified,Q1))$(EXTENSION_SINGLE)
endef
############## CONFIG FILE ##############

# 2. export the variable value as-is to the shell as an environment variable
export CONFIG_FILE
