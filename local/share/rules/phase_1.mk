# Copyright Michele Vidotto 2014 <michele.vidotto@gmail.com>


PRJ_NAME ?=

KMER ?=

MAXKMER ?=

REFERENCE ?=

FILTER_LENGTH ?=

# need for filter_contigs
context prj/abyss_assembly

logs:
	mkdir -p $@


ref.fasta.gz:
	ln -sf $(REFERENCE) $@

# should be set a litter larger than the real genome size, it is used to allocate memory
ref.len: ref.fasta.gz
	echo "$$(zcat $< | fasta_count -l) + 10000" \
	| bc >$@



# generate links and
# list reads used for kmers spectum construction
read.paired.lst:
	$(shell >$@) \
	$(foreach LIB,$(LIB_PAIRED), \
		$(foreach KEY,$(call keys,$(LIB)), \
			$(shell ln -sf $(call get,$(LIB),$(KEY)) $(notdir $(call get,$(LIB),$(KEY)))) \
			$(shell printf "$(notdir $(call get,$(LIB),$(KEY)))\n" >>$@) \
		) \
	)


read.unpaired.lst:
	$(shell >$@) \
	$(foreach LIB,$(LIB_UNPAIRED), \
		$(foreach KEY,$(call keys,$(LIB)), \
			$(shell ln -sf $(call get,$(LIB),$(KEY)) $(notdir $(call get,$(LIB),$(KEY)))) \
			$(shell printf "$(notdir $(call get,$(LIB),$(KEY)))\n" >>$@) \
		) \
	)



ifeq ($(EC),yes)
# produce kmers spectrum
# 2 output files:
#	*.freq.gz
#	*.freq.stat
$(PRJ_NAME).freq.gz: logs read.paired.lst read.unpaired.lst
	!threads
	$(call LOAD_MODULES); \
	KmerFreq_HA \   * Use when kmer size is larger than(>) 17bp *
	-p $(PRJ_NAME) \
	-t $$THREADNUM \   * Set thread number *
	-l <(cat $^2 $^3) \   * list of reads to be processed *
	$(call KMERFREQ_HA_VARIABLE_PART) \
	2>&1 \
	| tee $</KmerFreq_HA.$@.log


# correct paired reads
read.paired.lst.QC.xls: logs $(PRJ_NAME).freq.gz read.paired.lst
	!threads
	$(call LOAD_MODULES); \
	Corrector_HA \   * Use when kmer size is larger than(>) 17bp *
	-t $$THREADNUM \
	-j 1 \   * Set whether convert read1 and read2 corrected file into Pair-end file: 1 yes, 0 no *
	$(call CORRECTOR_HA_VARIABLE_PART) \
	$^2 $^3 \
	2>&1 \
	| tee $</Corrector_HA.$@.log


# correct unpaired reads
read.unpaired.lst.QC.xls: logs $(PRJ_NAME).freq.gz read.unpaired.lst
	!threads
	$(call LOAD_MODULES); \
	Corrector_HA \   * Use when kmer size is larger than(>) 17bp *
	-t $$THREADNUM \
	-j 0 \   * Set whether convert read1 and read2 corrected file into Pair-end file: 1 yes, 0 no *
	$(call CORRECTOR_HA_VARIABLE_PART) \
	$^2 $^3 \
	2>&1 \
	| tee $</Corrector_HA.$@.log
else
$(PRJ_NAME).freq.gz: logs read.lst
	touch $@

read.paired.lst.QC.xls: logs $(PRJ_NAME).freq.gz read.paired.lst
	touch $@

read.unpaired.lst.QC.xls: logs $(PRJ_NAME).freq.gz read.unpaired.lst
	touch $@
endif


# 3. reference it from the shell as an environment variable
soap.conf: read.paired.lst.QC.xls read.unpaired.lst.QC.xls
	@echo "$$CONFIG_FILE" >$@

# SOAPdenovo2 was compiled with the following parameters:
# make -j 4 'CATEGORIES=57' 'MAXKMERLENGTH=127' 'BIGASSEMBLY=1' 'OPENMP=1'


ifeq ($(PIPELINE),all)
# all
$(PRJ_NAME).scafSeq: logs soap.conf ref.len
	!threads
	@echo "*** ASSEMBLYNG WITH all ***";
	$(call LOAD_MODULES); \
	SOAPdenovo-127mer all \
	-s $^2 \   * configFile: the config file of solexa reads *
	-K $(KMER) \   * kmer(min 13, max 127): kmer size *
	-p $$THREADNUM \   * n_cpu: number of cpu for use *
	$(call SOAP_VARIABLE_PART) \
	-N $$(cat <$^3) \   * genomeSize: genome size for statistics *
	-o $(basename $@) \   * outputGraph: prefix of output graph file name *
	2>&1 \
	| tee $</SOAPdenovo-127mer.all.$@.log

$(PRJ_NAME).contig: $(PRJ_NAME).scafSeq
	touch $@
endif




# Steps:
#  pregraph -s soap.conf -K 63 -p 12 -R -o test
#  contig -g test -M 1 -R -s soap.conf -p 12 -m 127 -E
#  map -s soap.conf -g test -p 12 -K 63
#  scaff -g test -F -p 12 -N 201200 -V

ifeq ($(PIPELINE),sparse_pregraph)
# step 1a
$(PRJ_NAME).edge.gz: logs soap.conf ref.len
	!threads
	@echo "*** ASSEMBLYNG WITH sparse_pregraph ***";
	$(call LOAD_MODULES); \
	SOAPdenovo-127mer sparse_pregraph \
	-s $^2 \
	-K $(KMER) \
	-z $$(cat <$^3) \
	-R \
	-p $$THREADNUM \
	-o $(PRJ_NAME) \
	2>&1 \
	| tee $</SOAPdenovo-127mer.sparse_pregraph.$@.log
endif

ifeq ($(PIPELINE),pregraph)
# step 1b
$(PRJ_NAME).edge.gz: logs soap.conf ref.len
	!threads
	@echo "*** ASSEMBLYNG WITH pregraph ***";
	$(call LOAD_MODULES); \
	SOAPdenovo-127mer pregraph \
	-s $^2 \
	-K $(KMER) \
	-p $$THREADNUM \
	-R \
	-o $(PRJ_NAME) \
	2>&1 \
	| tee $</SOAPdenovo-127mer.sparse_pregraph.$@.log
endif

# simulation of logical OR
ifeq ($(PIPELINE),$(filter $(PIPELINE),sparse_pregraph pregraph))
# step 2
$(PRJ_NAME).contig: logs $(PRJ_NAME).edge.gz
	!threads
	@echo "*** ASSEMBLYNG WITH pregraph or sparse_pregraph ***";
	$(call LOAD_MODULES); \
	SOAPdenovo-127mer contig \
	-g $(PRJ_NAME) \
	-M 1 \
	-R \
	-s soap.conf \
	-p $$THREADNUM \
	-m  $(MAXKMER) \   * max k when using multi kmer *
	-E \   * merge clean bubble before iterate, works only if -M is set when using multi-kmer *
	2>&1 \
	| tee $</SOAPdenovo-127mer.contig.$@.log

# step 3
$(PRJ_NAME).readOnContig.gz: logs soap.conf $(PRJ_NAME).contig
	!threads
	$(call LOAD_MODULES); \
	SOAPdenovo-127mer map \
	-s $^2 \
	-g $(PRJ_NAME) \
	-p $$THREADNUM \
	-K $(KMER) \
	2>&1 \
	| tee $</SOAPdenovo-127mer.map.$@.log

# step 4
$(PRJ_NAME).scafSeq: logs $(PRJ_NAME).readOnContig.gz ref.len
	!threads
	$(call LOAD_MODULES); \
	SOAPdenovo-127mer scaff \
	-g $(PRJ_NAME) \
	-F \
	-p $$THREADNUM \
	-N $$(cat <$^3) \
	-V \
	2>&1 \
	| tee $</SOAPdenovo-127mer.scaff.$@.log
endif


# filter for scaffolds or contigs length
$(FILTER_LENGTH)bp.final.%.fasta: $(PRJ_NAME).%
	filter_contigs --fasta $< --length $(FILTER_LENGTH) >$@


# get statistics
$(FILTER_LENGTH)bp.final.%.gam: $(FILTER_LENGTH)bp.final.%.fasta
	$(call LOAD_MODULES); \
	gam-n50 $< \
	| tr \\t \\n >$@


.PHONY: test
test:
	@echo $(filter $(PIPELINE),sparse_pregraph pregraph)



# This should be the default target.
ifeq ($(PIPELINE),$(filter $(PIPELINE),sparse_pregraph pregraph))
ALL   +=  logs \
	  ref.fasta.gz \
	  ref.len \
	  read.paired.lst \
	  read.unpaired.lst \
	  $(PRJ_NAME).freq.gz \
	  read.paired.lst.QC.xls \
	  read.unpaired.lst.QC.xls \
	  soap.conf \
	  $(PRJ_NAME).edge.gz \
	  $(PRJ_NAME).contig \
	  $(PRJ_NAME).readOnContig.gz \
	  $(PRJ_NAME).scafSeq \
	  $(FILTER_LENGTH)bp.final.contig.fasta \
	  $(FILTER_LENGTH)bp.final.scafSeq.fasta \
	  $(FILTER_LENGTH)bp.final.contig.gam \
	  $(FILTER_LENGTH)bp.final.scafSeq.gam
endif

ifeq ($(PIPELINE),all)
ALL   +=  logs \
	  ref.fasta.gz \
	  ref.len \
	  read.paired.lst \
	  read.unpaired.lst \
	  $(PRJ_NAME).freq.gz \
	  read.paired.lst.QC.xls \
	  read.unpaired.lst.QC.xls \
	  soap.conf \
	  $(PRJ_NAME).scafSeq \
	  $(FILTER_LENGTH)bp.final.contig.fasta \
	  $(FILTER_LENGTH)bp.final.scafSeq.fasta \
	  $(FILTER_LENGTH)bp.final.contig.gam \
	  $(FILTER_LENGTH)bp.final.scafSeq.gam
endif

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=



# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(wildcard $(PRJ_NAME).*) \
	 $(wildcard edge_*)

# add dipendence to clean targhet
clean: clean_seqs

# clean all about error corrected reads
.PHONY: clean_seqs
clean_seqs:
	@echo cleaning up...
	$(foreach LIB,$(LIB_PAIRED) $(LIB_UNPAIRED), \
		$(foreach KEY,$(call keys,$(LIB)), \
			$(shell rm -rf $(notdir $(call get,$(LIB),$(KEY)))*) \
		) \
	)